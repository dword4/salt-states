base:
  '*':
    - vim
    - users
    - snmp
    - repo
    - packages
    - selinux
    - firewalld
    - lnms
    - updatehostname
    - reboot
