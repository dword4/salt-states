# this will perform a curl on the target minion
# normal curl flags can be applied, very handy
do_a_curl:
  cmd.run:
    - name: >- 
        curl -k -v -H 'someheader' -u 'user:password' "https://192.168.1.65/api/some/end_point"  --data-binary "/api/discovery_session/1"

