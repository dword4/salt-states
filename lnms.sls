# this will perform a curl on the target minion
do_a_curl:
  cmd.run:
    - name: >- 
        curl -X POST --data-binary '{"hostname":"{{ salt['grains.get']('ip4_interfaces:eth0:0') }}","force_add":"true","port":"161","version":"v2c","community":"public"}' -k -v -H 'X-Auth-Token: abb9c2c4f9586f38ac9e0dea0c7ef2b4' 'http://192.168.1.85/api/v0/devices'

