selinux_mode:
  cmd:
    - run
    - name: setenforce 0

  file.replace:
    - name: /etc/selinux/config
    - pattern: "SELINUX=enforcing"
    - repl: "SELINUX=disabled" 
