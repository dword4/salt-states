# salt-states

salt-states from the home lab environment
```
base:
  '*':
    - vim
    - users
    - snmp
    - repo
    - packages
    - selinux
    - firewalld
    - lnms
    - updatehostname
    - reboot
```
