updates:
  pkgrepo.managed:
    - humanname: CentOS-$releasever - Updates
    - baseurl: http://192.168.1.50/centos/7/updates
    - gpgcheck: 1
    - gpgkey: file:///etc/pki/rpm-gpg/RPM_GPG-KEY-CentOS-7
